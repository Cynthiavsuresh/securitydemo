package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.ApiResponse;
import com.example.demo.dto.Userdto;
import com.example.demo.service.UserService;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;

@RestController
@SecurityRequirement(name = "userapi")
public class UserController {
	@Autowired
	UserService userservice;
	
	@PostMapping("/register")
	public ResponseEntity<ApiResponse> register(@Valid @RequestBody Userdto userdto){
		return ResponseEntity.status(HttpStatus.CREATED).body(userservice.register(userdto));
	}

}
