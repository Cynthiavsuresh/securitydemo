package com.example.demo.service.Implementation;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.ApiResponse;
import com.example.demo.dto.Userdto;
import com.example.demo.entity.User;
import com.example.demo.exception.ResourceConflictExists;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	UserRepository userrepository;

	@Override
	public ApiResponse register(Userdto userdto) {
		Optional<User> reg = userrepository.findByEmail(userdto.getEmail());
		if (reg.isPresent()) {
			throw new ResourceConflictExists("User already Register in this address ");
		}

		User reg1 = User.builder().name(userdto.getName()).email(userdto.getEmail()).phone(userdto.getPhone()).build();
		userrepository.save(reg1);
		return ApiResponse.builder().code(200).message("User register succefully").build();

	}

}
