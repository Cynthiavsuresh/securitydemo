package com.example.demo.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Userdto {
	
	@NotNull(message="Name is required")
	@Size(min = 3,message = "Name should have atleast 3 character")
	private String name;
	@NotNull(message="Email is required")

	@Email(regexp = "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}")
	private String email;
	@NotBlank(message="Phone number is required")
	@Pattern(regexp = "[6789][0-9]{9}", message = "Enter the valid Contact Number")
	private String phone;
}
